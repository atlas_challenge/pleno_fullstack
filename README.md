<h1 align="center">
    <img alt="GoStack" src="https://media-exp1.licdn.com/dms/image/C4E0BAQFPMdEVgnhuKA/company-logo_200_200/0/1620547479668?e=2147483647&v=beta&t=fyHhUS_HofLVmWczMvNJ72tYKwnKOhE1YbKr6jCYFPk" width="200px" />
</h1>

<h3 align="center">
  Desafio Pleno FullStack: Conceitos do NodeJS e ReactJS
</h3>

<p align="center">“Sua única limitação é você mesmo”!</blockquote>

<p align="center">
  <a href="https://rocketseat.com.br">
    <img alt="Made by AtlasGR" src="https://img.shields.io/badge/made%20by-AtlasGR-%2304D361">
  </a>
</p>

<p align="center">
  <a href="#rocket-sobre-o-desafio">Sobre o desafio</a>&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;
  <a href="#-entrega">Entrega</a>
</p>

## :rocket: Sobre o desafio

Crie uma aplicação de lista de amigos com autenticação.

<br>

## Frontend 

Será utilizado ReactJS, em qualquer versão, de preferencia em typescript.

Para otimização de tempo sugerimos a utilização do framework de estilo [tailwindUI](https://tailwindui.com).
- Pode ser utilizado o seguinte modelo para Login > [link](https://tailwindui.com/components/application-ui/forms/sign-in-forms).
- Pode ser utilizado cards para exibição de amigos > [link](https://v1.tailwindcss.com/components/cards)

#### Exemplo:
<h1 align="center">
    <img alt="GoStack" src="https://tailwindui.com/img/components/sign-in-forms.01-simple-card-xl.png" width="400px" />
</h1>

<br>

#### Serão duas páginas:
- Login;
- Listagem de Amigos;

<br>

### Requisições:

Serão executadas duas chamadas a API, nas quais:
- A primeira requisição irá enviar os dados deste usuário e receber um código randomico gerado pelo backend e armazenar este código em Cookie ou Sessão;

- A segunda requisição irá enviar o código armazenado em cookie/sessão para o backend e receber uma lista de amigos e exibir em cards (layout fica a seu critério);

<br>

## Backend 

Utilizando NodeJS, em qualquer versão, de preferencia com typescript e com a biblioteca [Express](https://www.npmjs.com/package/express), iremos criar uma rota que será a de login e outra de listagem de usuários.

### Rotas

- `POST /login`: A rota deve receber `email` e `senha` dentro do corpo (BODY); Certifique-se de enviar tanto o email quanto a senha no formato string com aspas duplas. Quando receber esta chamada de login, devolver um código randomico para o frontend;

Request:
```js
{
    email: "exemplo@exemplo.com", //string
    senha: "senha123" //string
}
```

Response:
```js
{
    success: true, //booleano
    token: "1234" //string
}
```

- `GET /list-friends`: A rota deve receber o código gerado anteriormente que estava salvo em Cookie ou Session no HEADER e devolver a listagem de usuários, para isto, usaremos uma API de apoio. Quando receber a requição, realizar uma chamada na API (https://jsonplaceholder.typicode.com/users) para buscar os dados e devolver para o frontend a listagem.

Request:
```js
    HEADER: 
        Token: "1234"
```

Response:
```js
{
  {
    "id": 1,
    "name": "Leanne Graham",
    "username": "Bret",
    "email": "Sincere@april.biz",
    "address": {
      "street": "Kulas Light",
      "suite": "Apt. 556",
      "city": "Gwenborough",
      "zipcode": "92998-3874",
      "geo": {
        "lat": "-37.3159",
        "lng": "81.1496"
      }
    },
    "phone": "1-770-736-8031 x56442",
    "website": "hildegard.org",
    "company": {
      "name": "Romaguera-Crona",
      "catchPhrase": "Multi-layered client-server neural-net",
      "bs": "harness real-time e-markets"
    }
  },
  {
    "id": 2,
    ...
  },
  ...
}
```


<br>

## 📅 Entrega

- Este desafio deverá ser entregue em seu repositório pessoal no GitHub e seu link enviado diretamente a quem lhe forneceu este teste. 
- O prazo máximo é de 48hrs.
- Desejável que sua aplicação esteja rodando em alguma plataforma como [Heroku](https://www.heroku.com/) ou similar durante o período de avaliação.

<br>

## :memo: Licença

Esse projeto está sob a licença MIT. 

---

<br>

Feito com ♥ by AtlasGR 
